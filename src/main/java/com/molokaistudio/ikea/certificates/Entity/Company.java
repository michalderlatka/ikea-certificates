package com.molokaistudio.ikea.certificates.Entity;

public class Company {
    private String companyName;
    private String nossNumber;
    private String bceNumber;
    private String address;
    private String result;
    private String date;

    public Company(String companyName, String nossNumber, String bceNumber, String address, String result, String date) {
        this.companyName = companyName;
        this.nossNumber = nossNumber;
        this.bceNumber = bceNumber;
        this.address = address;
        this.result = result;
        this.date = date;
    }

    public Company() {
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getNossNumber() {
        return nossNumber;
    }

    public void setNossNumber(String nossNumber) {
        this.nossNumber = nossNumber;
    }

    public String getBceNumber() {
        return bceNumber;
    }

    public void setBceNumber(String bceNumber) {
        this.bceNumber = bceNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
