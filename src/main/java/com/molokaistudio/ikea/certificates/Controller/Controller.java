package com.molokaistudio.ikea.certificates.Controller;

import com.molokaistudio.ikea.certificates.Entity.Company;
import com.molokaistudio.ikea.certificates.Model.CsvBuilder;
import com.molokaistudio.ikea.certificates.Model.PdfCreator;
import com.molokaistudio.ikea.certificates.Model.VatIdProvider;
import com.molokaistudio.ikea.certificates.Task.EservicesRest.EservicesRestResponse;
import com.molokaistudio.ikea.certificates.Task.EservicesTask;
import com.molokaistudio.ikea.certificates.Task.SocialsecurityTask;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ResourceBundle;

import static java.nio.charset.Charset.forName;

public class Controller implements Initializable {

    @FXML TextArea logTextArea;

    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    @FXML protected void handleStartButton(ActionEvent event) {
        new Thread(task).start();
    }

    private Task<Void> task = new Task<Void>() {
        @Override
        protected Void call() throws Exception {
            String[] vatIdsToProcess = VatIdProvider.getVatIds();

            EservicesTask eservicesTask = new EservicesTask();
            SocialsecurityTask socialsecurityTask = new SocialsecurityTask();
            CsvBuilder csvBuilder = new CsvBuilder();
            PdfCreator pdfCreator = new PdfCreator();

            int counter = 1;
            int toProcess = vatIdsToProcess.length;

            Platform.runLater(() -> logTextArea.appendText("Liczba nip-ów do przetworzenia: " + String.valueOf(toProcess) + "\n"));

            File directory = new File("pdf");
            if (!directory.exists()) {
                directory.mkdir();
            }

            for (final String vatId : vatIdsToProcess) {
                final String line = String.valueOf(counter) + "/" + String.valueOf(toProcess);

                Platform.runLater(() -> logTextArea.appendText(line + " " + vatId + "\n"));

                try {
                    Company company = socialsecurityTask.process(vatId);

                    EservicesRestResponse eservicesRestResponse = eservicesTask.getEnterpriseData(vatId);

                    int attempts = 0;
                    boolean downloaded = false;

                    while(attempts++ < 5 && !downloaded) {
                        try {
                            eservicesTask.downloadAttestPdf(eservicesRestResponse.getAttestNumber(), vatId);
                            downloaded = true;
                        } catch (Exception e) {
                            Platform.runLater(() -> logTextArea.appendText("Błąd pobierania pdf fisc - próbuję jeszcze raz\n"));
                        }
                    }
                    pdfCreator.renderPdf(company, vatId);

                    FileWriter fw = new FileWriter("result.csv", true);
                    fw.append(csvBuilder.append(vatId, eservicesRestResponse, downloaded, company));
                    fw.close();
                } catch (Exception e) {
                    Platform.runLater(() -> logTextArea.appendText(e.getMessage() + "\n"));
                }

                counter++;
            }



            Platform.runLater(() -> logTextArea.appendText(" KONIEC !!!"));

            return null;
        }
    };
}
