package com.molokaistudio.ikea.certificates.Model;

import com.molokaistudio.ikea.certificates.Entity.Company;
import org.apache.commons.io.IOUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class PdfCreator {

    private static final String NEE_TEXT = "Inhoudingsplicht sociale zekerheid: <b>NEE</b>.<br />Beslissing geldig tot <u>%date%</u>";
    private static final String JA_TEXT = "Inhoudingsplicht sociale zekerheid: <b>JA</b>.";
    private static final String NA_TEXT = "Geen gegevens beschikbaar over de inhoudingsplicht.";
    private static final String NEE_IMG = "https://www.socialsecurity.be/app005/ppr/faces/javax.faces.resource/logo/situation-ok-nl.png?ln=img";
    private static final String JA_IMG = "https://www.socialsecurity.be/app005/ppr/faces/javax.faces.resource/logo/situation-nook-nl.png?ln=img";
    private static final String NA_IMG = "https://www.socialsecurity.be//app005/ppr/faces/javax.faces.resource/logo/situation-nodata-nl.png?ln=img";

    private String htmlContent;

    public PdfCreator() {
        try {
            InputStream is = getClass().getResourceAsStream("/source.html");
            htmlContent = IOUtils.toString(is, StandardCharsets.UTF_8);
        } catch (Exception e) {

        }
    }

    public void renderPdf(Company company, String vatId) throws Exception {
        String result = htmlContent;
        result = result.replace("%name%", company.getCompanyName());
        result = result.replace("%rsz%", company.getNossNumber());
        result = result.replace("%kbo%", company.getBceNumber());
        result = result.replace("%adres%", company.getAddress());

        if (company.getResult().equals("NEE")) {
            result = result.replace("%image%", NEE_IMG);
            result = result.replace("%text%", NEE_TEXT.replace("%date%", company.getDate()));
        } else if (company.getResult().equals("JA")) {
            result = result.replace("%image%", JA_IMG);
            result = result.replace("%text%", JA_TEXT);
        } else {
            result = result.replace("%image%", NA_IMG);
            result = result.replace("%text%", NA_TEXT);
        }

        String outputFile = "pdf/" + vatId + "_rsz.pdf";

        try (OutputStream os = new FileOutputStream(outputFile)) {
            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(result);
            renderer.layout();
            renderer.createPDF(os);
        } catch (Exception e) {
            throw new Exception("Błąd podczas tworzenia pdf-a");
        }
    }
}
