package com.molokaistudio.ikea.certificates.Model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class VatIdProvider {
    public static String[] getVatIds() {
        List<String> vatIds = new ArrayList<>();

        File file = new File("nip.txt");

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                vatIds.add(line.trim());
            }
        } catch (Exception e) {

        }

        return vatIds.toArray(new String[0]);
    }
}
