package com.molokaistudio.ikea.certificates.Model;

import com.molokaistudio.ikea.certificates.Entity.Company;
import com.molokaistudio.ikea.certificates.Task.EservicesRest.EservicesRestResponse;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringJoiner;

public class CsvBuilder {
    private SimpleDateFormat format;

    public CsvBuilder() {
        format = new SimpleDateFormat("dd-MM-yyyy");
    }

    public String append(String vatId, EservicesRestResponse eservicesRestResponse, boolean eservicesPdfDownloaded, Company company) {
        StringJoiner sj = new StringJoiner("\",\"", "\"", "\"");
        sj.add(vatId);

        sj.add(company.getCompanyName());
        if (company.getResult().equals("NEE")) {
            sj.add(company.getDate() != null ? formatDate(company.getDate()) : "");
        } else {
            sj.add(company.getResult().equals("JA") ? "NOT OK" : company.getResult());
        }

        if (eservicesRestResponse.isHasDebt()) {
            sj.add("NOT OK");
        } else {
            sj.add(eservicesRestResponse.getValidUntil() != null ? eservicesRestResponse.getValidUntil() : "N/A");
        }

        return sj.toString() + System.lineSeparator();
    }

    private String formatDate(String date) {
        try {
            Date d = format.parse(date);
            return DateFormatUtils.format(d, "yyyy-MM-dd");
        } catch (Exception e) {
            return "N/A";
        }
    }
}
