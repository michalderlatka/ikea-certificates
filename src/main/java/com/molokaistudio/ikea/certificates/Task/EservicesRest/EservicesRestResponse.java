package com.molokaistudio.ikea.certificates.Task.EservicesRest;

public class EservicesRestResponse {
    private String validUntil;
    private String attestNumber;
    private boolean hasDebt;
    private String enterpriseName;
    private boolean canForce;
    private boolean enterpriseActive;

    public EservicesRestResponse(String validUntil, String attestNumber, boolean hasDebt, String enterpriseName, boolean canForce, boolean enterpriseActive) {
        this.validUntil = validUntil;
        this.attestNumber = attestNumber;
        this.hasDebt = hasDebt;
        this.enterpriseName = enterpriseName;
        this.canForce = canForce;
        this.enterpriseActive = enterpriseActive;
    }

    public EservicesRestResponse() {
    }

    public String getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(String validUntil) {
        this.validUntil = validUntil;
    }

    public String getAttestNumber() {
        return attestNumber;
    }

    public void setAttestNumber(String attestNumber) {
        this.attestNumber = attestNumber;
    }

    public boolean isHasDebt() {
        return hasDebt;
    }

    public void setHasDebt(boolean hasDebt) {
        this.hasDebt = hasDebt;
    }

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName;
    }

    public boolean isCanForce() {
        return canForce;
    }

    public void setCanForce(boolean canForce) {
        this.canForce = canForce;
    }

    public boolean isEnterpriseActive() {
        return enterpriseActive;
    }

    public void setEnterpriseActive(boolean enterpriseActive) {
        this.enterpriseActive = enterpriseActive;
    }
}
