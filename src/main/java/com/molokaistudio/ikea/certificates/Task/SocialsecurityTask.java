package com.molokaistudio.ikea.certificates.Task;

import com.molokaistudio.ikea.certificates.Entity.Company;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.Map;

public class SocialsecurityTask {

    private final static String URL = "https://www.socialsecurity.be/app005/ppr/faces/html/consultRetainment.xhtml";

    private Map<String, String> cookies;

    private String viewState;

    public SocialsecurityTask() throws Exception {
        try {
            Connection.Response response = Jsoup.connect(URL + "?language=nl_BE").method(Connection.Method.GET).execute();
            cookies = response.cookies();

            Document document = response.parse();
            viewState = getViewStateFromDocument(document);
        } catch (Exception e) {
            throw new Exception("Nie mogłem połączyć się ze stroną");
        }
    }

    public Company process(String vatId) {
        try {
            Connection.Response postResponse = Jsoup.connect(URL)
                    .data("ConsultRetainment", "ConsultRetainment")
                    .data("ConsultRetainment:bce", vatId)
                    .data("ConsultRetainment:consult", "Consulteren")
                    .data("javax.faces.ViewState", viewState)
                    .method(Connection.Method.POST)
                    .cookies(cookies)
                    .execute();

            Document resultDocument = postResponse.parse();

            viewState = getViewStateFromDocument(resultDocument);

            Element companyName = resultDocument.getElementById("PrepareRetainment:contractorDenomination");
            Element bceNumber = resultDocument.getElementById("PrepareRetainment:contractorBceNumber");
            Element nossNumber = resultDocument.getElementById("PrepareRetainment:contractorNossNumber");
            Element postCode = resultDocument.getElementById("PrepareRetainment:contractorAddressPostalCode");
            Element city = resultDocument.getElementById("PrepareRetainment:contractorAddressCity");
            Element result = resultDocument.selectFirst(".retainmentTextDetail > span > strong");
            Element date = resultDocument.selectFirst(".retainmentTextDetail > span > u");

            Company company = new Company();
            company.setCompanyName(companyName != null ? companyName.html() : "Geen gegevens");
            company.setBceNumber(bceNumber.html());
            company.setNossNumber(nossNumber != null ? nossNumber.html() : "Geen gegevens");

            if (postCode == null && city == null) {
                company.setAddress("Geen gegevens");
            } else {
                String addressLine = postCode != null ? postCode.html() + " " : "";
                company.setAddress(addressLine.concat(city != null ? city.html() : ""));
            }

            company.setResult(result != null ? result.html() : "N/A");

            if (date != null) {
                company.setDate(date.html());
            }

            return company;
        } catch (Exception e) {
            return new Company();
        }
    }

    private String getViewStateFromDocument(Document document) {
        Element element = document.getElementById("javax.faces.ViewState");
        return element.val();
    }
}
