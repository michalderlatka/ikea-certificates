package com.molokaistudio.ikea.certificates.Task;

public interface ITask {
    boolean process(String vatId);
}
