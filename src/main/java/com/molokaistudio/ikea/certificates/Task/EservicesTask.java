package com.molokaistudio.ikea.certificates.Task;

import com.google.gson.Gson;
import com.molokaistudio.ikea.certificates.Task.EservicesRest.EservicesRestResponse;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class EservicesTask {

    private static final String URL = "https://eservices.minfin.fgov.be/mym-api-rest/crassus/pdf?attestId=%s";

    private static final String JSON_URL = "https://eservices.minfin.fgov.be/mym-api-rest/crassus/attest?enterpriseNumber=%s";

    private HttpClient httpClient;

    public EservicesTask() {
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(5000)
                .setConnectionRequestTimeout(3000)
                .setSocketTimeout(10000)
                .build();

        httpClient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build();
    }

    private String buildUrl(String attestId) {
        return String.format(URL, attestId);
    }

    private String buildJsonUrl(String vatId) {
        return String.format(JSON_URL, vatId);
    }

    public void downloadAttestPdf(String attestNumber, String vatId) throws Exception {
        if (attestNumber == null) {
            return;
        }

        HttpGet request = new HttpGet(buildUrl(attestNumber));
        HttpResponse response = httpClient.execute(request);

        Header[] headers = response.getHeaders("Content-Type");
        for (Header header : headers) {
            if (header.getValue().equals("application/json")) {
                return;
            }
        }

        try (
                InputStream is = response.getEntity().getContent();
                FileOutputStream fos = new FileOutputStream(new File("pdf/" + vatId + "_fisc.pdf"))
        ) {
            int length = -1;
            byte[] buffer = new byte[2048];// buffer for portion of data from connection
            while ((length = is.read(buffer)) > -1) {
                fos.write(buffer, 0, length);
            }
        } catch (Exception e) {
            throw new Exception("Nie można pobrać pdf-a");
        }
    }

    public EservicesRestResponse getEnterpriseData(String vatId) {
        String serviceResponse;

        try (InputStream in = new URL(buildJsonUrl(vatId)).openStream()) {
            serviceResponse = IOUtils.toString(in, StandardCharsets.UTF_8);
            Gson gson = new Gson();

            return gson.fromJson(serviceResponse, EservicesRestResponse.class);
        } catch (Exception e) {
            return new EservicesRestResponse();
        }
    }
}
